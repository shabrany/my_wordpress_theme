<?php 


class PortfolioTheme {
    
    private $locations = array();

    private $prefix_portfolio = 'portfolio';
    
    public function __construct() {
        
        $this->add_location('main-navigation', 'This is the main navigation menu');
        $this->add_location('footer-nav', 'This menu is displayed at the bottom of the page');                
        
        // Register all defined nav locations
       //add_action('init', array($this, 'register_menus'));        
        
        
        // Enable
        $this->enable_thumbnails();   

        // Add sizes for portfolio thumbs
        //add_action('after_setup_theme', array($this, 'add_custom_size'));        
       
    }
    
    /**
     * Register menus that are collected in Locations
     */
    public function register_menus() {
        register_nav_menus($this->locations);
    }
    
    /**
     * Add menu locations
     */
    public function add_location($name, $desc) {
        $this->locations[$name] = __($desc);                
    }
    
    /**
     * Add support for post thumbnails
     */
    public function enable_thumbnails() {
        add_theme_support( 'post-thumbnails', array('portfolio')); 
        set_post_thumbnail_size( 300, 300, true );        
    }

    /**
     * Method permits add custom sizes for specific thumbnails
     */
    public function add_custom_size() {
        add_image_size('portfolio-thumb', 250, 250, true);
    }
        
}

// $pf_theme = new PortfolioTheme();

//   define('MY_POST_TYPE', 'my');
//   define('MY_POST_SLUG', 'gallery');

//   function my_register_post_type () {
//     $args = array (
//         'label' => 'Gallery',
//         'supports' => array( 'title', 'editor', 'excerpt' ),
//         'register_meta_box_cb' => 'my_meta_box_cb',
//         'show_ui' => true,
//         'query_var' => true
//     );
//     register_post_type( MY_POST_TYPE , $args );
//   }
//   add_action( 'init', 'my_register_post_type' );

//   function my_meta_box_cb () {
//     add_meta_box( MY_POST_TYPE . '_details' , 'Media Library', 'my_meta_box_details', MY_POST_TYPE, 'normal', 'high' );
//   }

//   function my_meta_box_details () {
//     global $post;
//     $post_ID = $post->ID; // global used by get_upload_iframe_src
//     printf( "<iframe frameborder='0' src=' %s ' style='width: 100%%; height: 400px;'> </iframe>", get_upload_iframe_src('media') );
//   }