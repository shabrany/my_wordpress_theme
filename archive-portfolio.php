<?php get_header() ?>

<?php
$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 10 );
$loop = new WP_Query( $args );
?>

<div id="primary" class="container">

    <div class="pf-wrapper">
        
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <a href="<?php the_permalink() ?>">
        <div class="item-outer">            
            <div class=""><?php the_post_thumbnail('portfolio-thumb') ?></div>      
            <h2><?php the_title() ?></h2>  
        </div>   
        </a>            

        <?php endwhile; // end of the loop. ?>

    </div>           

</div><!-- #primary -->
<?php get_footer() ?>