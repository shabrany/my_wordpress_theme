<?php get_header() ?>

<div class="container">
<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
	<h1 class="entry-title"><?php the_title(); ?></h1>				
	<div class="entry-content">
		<?php the_excerpt(); ?>
		<a href="<?php the_permalink() ?>">Lees meer</a>
	</div><!-- .entry-content -->
	<?php endwhile; else: ?>
	<h2>Woops...</h2>
	<p>Sorry, no posts we're found.</p>
<?php endif; ?>
</div>

<?php get_footer() ?>