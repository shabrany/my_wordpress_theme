<!DOCTYPE html>
<html>
<head>
	<title><?php bloginfo('name') . ' ' . wp_title() ?></title>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>">
	<?php wp_head() ?>
	<script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/jquery-1.7.1.min.js"></script>
</head>
<body>

	<header class="container">
		<section id="logo">
			<h2><?php bloginfo('name') ?></h2>
		</section>

		<?php if (is_front_page()): ?>>
		<!-- jQuery Slideshow -->
		<section id="slideshow" class="accordion">
			<ul id="featured">
				<li>
					<image src="<?php bloginfo('template_url') ?>/gallery/pool.jpg" alt="Vintage Pool" />
				</li>
				<li>
					<image src="<?php bloginfo('template_url') ?>/gallery/gas.jpg" alt="Retro Gas Pump" />
				</li>
				<li>
					<image src="<?php bloginfo('template_url') ?>/gallery/car.jpg" alt="Old Car Steering Wheel" />
				</li>
			</ul>
		</section>
	<?php endif; ?>

		<?php wp_nav_menu( array( 
			'theme_location' => 'main-navigation', 
			'depth' => 2, 
			'container_class' => 'top-nav') 
		); ?>
	</header>
