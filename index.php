<?php get_header() ?>

    <div id="primary" class="container">

        <?php while ( have_posts() ) : the_post(); ?>

            <h1><?php the_title() ?></h1>

            <?php the_content(); ?>

        <?php endwhile; // end of the loop. ?>


    </div><!-- #primary -->

<?php get_footer() ?>